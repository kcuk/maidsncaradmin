
import { AdminService } from '../admin.service';
import { CommonService } from './../../shared/services/common.service';
import { AuthService } from './../../core/auth/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { AttendanceService } from './attendance.service';


@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  societyAdmins: any;
  inProgress = false;

  totalRecords = 0;
  filterBy = 'name';
  searchedTerm = '';
  date:any;
  year:any;day:any;month:any={no:'',name:''};
  dayDate:any=[
    {day:'mon',date:'1-01-19'},
    {day:'tue',date:'2-01-19'},
    {day:'wed',date:'3-01-19'},
    {day:'thurs',date:'4-01-19'},
    {day:'fri',date:'5-01-19'},
    {day:'sat',date:'6-01-19'},
    {day:'sun',date:'7-01-19'},
    {day:'mon',date:'8-01-19'},
    {day:'tues',date:'9-01-19'},
  ];

  

  public importErrorMessage = '';
  colDefs = [
    {
      name: 'Helper_name',
      displayName: 'Helper Name',
      width: '10px',
      maxwid:'80px',
      marginl:'2%'
    },
    {
      name: 'Service_Provide',
      displayName: 'Service Provide',
      width: '10px',
      maxwid:'80px',
      marginl:'2%'
    },
    {
      name: 'Service_Type',
      displayName: 'Service Type',
      width: '10px',
      maxwid:'80px',
      marginl:'2%'
    },
    {
      name: 'flat_type',
      displayName: 'Flat No',
      maxwid:'80px',
      marginl:'2%'
   
    }
  ];

  displayedColumns = this.colDefs.map(c => c.name);
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  constructor(
    private _authService: AuthService,
    private _commonService: CommonService,
    private _adminService: AdminService,
    private _router: Router,
    private _attendanceService:AttendanceService
    // private _datePipe: DatePipe
  ) {

  }

  ngOnInit() {
    this.getSocietyAdminList();
    this.displayedColumns.push('action');
    this._adminService.selectedSocietyAdmin = {};

    //to get the date 
    this.date = new Date();
     this.year = this.date.getFullYear();
    this.month.no = this.date.getMonth();
    this.monthInCalenderFunc(this.month.no);
    
    // this.calenderMonth(this.month);


// console.log(this.month);
// this.day = this.date.getDay();console.log(this.day, "-------",this.date.getDay());


  }

  // applyFilter() {
  //   this.paginator.pageIndex = 0;
  //   this.getSocietyAdminList();
  // }


  monthInCalenderFunc(mon){
    if(mon == 0){
      return this.month.name = 'january';
    }
    else if(mon == 1){
      return this.month.name = 'febraury';
    }
    else if(mon == 2){
      return this.month.name = 'march';
    }
    else if(mon == 3){
      return this.month.name = 'april';
    }
    else if(mon == 4){
      return this.month.name = 'may';
    }
    else if(mon == 5){
      return this.month.name = 'june';
    }
    else if(mon == 6){
      return this.month.name = 'july';
    }
    else if(mon == 7){
      return this.month.name = 'august';
    }
    else if(mon == 8){
      return this.month.name = 'september';
    }
    else if(mon == 9){
      return this.month.name = 'october';
    }
    else if(mon == 10){
      return this.month.name = 'november';
    }
    else if(mon == 11){
      return this.month.name = 'december';
    }

    else {
      return mon;
    }
    
  }
  
  backwardClick(){
    this.month.no--;
    this.monthInCalenderFunc(this.month.no);
    if(this.month == 0){
      this.year--;
      console.log("month in backward click ",this.month);
    }
  }

  forwardClick(){
    console.log("month in forward click before ++",this.month.no);
    this.month.no++;
    this.monthInCalenderFunc(this.month.no);
    console.log("month in forward click after ++",this.month.no,"----------month name---------",this.month.name);
  }

//to perform event on click event of a Report Button
  getReport(){
    console.log("report generation process");
    this._attendanceService.getReport();
  }
  







  
  getSocietyAdminList() {
    this.inProgress = true;
    const options = {
      // params: {
      //   page: this.paginator.pageIndex + 1,
      //   search_by: this.filterBy,
      //   search: this.searchedTerm
      // }
    };
    this._adminService.getSocietyAdminList(
      options,
      res => {
        const resultArray = res.data;
        // this.totalRecords = res.count;
        this.societyAdmins = new MatTableDataSource<any>(resultArray);
        this.societyAdmins.paginator = this.paginator;
        this.inProgress = false;
      },
      err => {
        this.inProgress = false;
      }
    );
  }

  // public getFormattedDate(elem) {
  //   return this._datePipe.transform(elem, 'MM-dd-yyyy');
  // }

  public addSocietyAdmin() {
    this._commonService.gotoPage('add-society-admin');
    // this._router.navigate(['/admin/add-society-admin']);
  }

  public editSocietyAdmin(societyAdmin) {
    this._adminService.fetchSocietyAdminById(
      societyAdmin.id,
      res => {
        this._adminService.selectedSocietyAdmin = res.data;
        this._commonService.gotoPage('edit-society-admin');
      },
      err => {}
    );
  }

  public deleteSocietyAdmin(societyAdmin) {
    this._commonService.openConfirmDialog(
      {
        title: 'Delete Society Admin',
        content: 'Are you sure to delete this society admin?'
      },
      () => {
        this._adminService.deleteSocietyAdminById(
          societyAdmin.id,
          data => {
            this.getSocietyAdminList();
            this._commonService.showMessage(
              'Society Admin has been deleted successfully'
            );
          },
          err => {}
        );
      }
    );
  }
}
