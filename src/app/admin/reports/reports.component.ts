import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  ID:string;
  NAME: string;
  AGE: number;
  CURRENT_ADDRESS: string;
  TOTALTOWERS: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H',ID:'',NAME:'',AGE: 4,CURRENT_ADDRESS:'',TOTALTOWERS:''},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He',ID:'',NAME:'',AGE: 4,CURRENT_ADDRESS:'',TOTALTOWERS:''}
];

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // table 
  displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol','ID', 'NAME', 'AGE', 'CURRENT_ADDRESS', 'TOTALTOWERS'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
