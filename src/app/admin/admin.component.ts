import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  public user: any;
  public navs: any;

  alerts = [];
  alertCount = 0;
  alertsInterval: any;
  constructor(
  ) {
    this.navs = [
      // {
      //   title: 'Dashboard',
      //   url: '/admin/dashboard',
      //   icon: 'glyphicon glyphicon-th-large',
      //   enable: true,
      //   visible: true,
      //   subNavs: []
      // },
      {
        title: 'Society',
        url: '/admin/manage-society',
        icon: '../../../assets/img/society.svg',
        enable: true,
        visible: true,
        subNavs: []
      },
      {
        title: 'Resident',
        url: '/admin/manage-resident',
        icon: '../../../assets/img/resident.svg',
        enable: true,
        visible: true,
        subNavs: []
      },
      {
        title: 'Helper',
        url: '/admin/manage-employee',
        icon: '../../../assets/img/helper.png',
        enable: true,
        visible: true,
        subNavs: []
      },
      {
        title: 'Society Admin',
        url: '/admin/manage-society-admin',
        icon: '../../../assets/img/society-admin.png',
        enable: true,
        visible: true,
        subNavs: []
      },
      {
        title:'Attendance',
        url:'/admin/attendance',
        icon:'../../../assets/img/attendance.png',
        icon2: '../../../assets/img/resident.svg',
        enable:true,
        visible:true,
        subNavs:[]
      },
      {
        title:'Reports',
        url:'/admin/reports',
        icon:'../../../assets/img/reports.png',
        enable:true,
        visible:true,
        subNavs:[]
      },
      {
        title: 'Replacement',
        url: '/admin/requests/replacement',
        //icon: '../../../assets/img/society.svg',
        enable: true,
        visible: true,
        subNavs: [
        {
        title: 'Shortlisted',
        url: '/admin/requests/shorlisted',
        // icon: '../../../assets/img/society.svg',
        enable: true,
        visible: true,
        
        }, 
        {
          title: 'sted',
          url: '/admin/requests/shorlisted',
          // icon: '../../../assets/img/society.svg',
          enable: true,
          visible: true,
          
          }, 
        ]
        }
    ];
  }

  ngOnInit() {
  }

  markAsRead(event) {}
  ngOnDestroy() {
  }
}
